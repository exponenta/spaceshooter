﻿using UnityEngine;
using UniRx;

public class LevelConfigure : MonoBehaviour
{
    public static LevelConfigure instance;

    // Почему не Observable ?? по хорошему да, нужно в них, но сска, они не сериализуются Unity
    // и не отоброжаются в Едиторе. Бесит!
    // как кстати и Properties.
    //
    [System.Serializable]
    public class LevelConfig
    {
        // кастыль, так как я хочу настроить в редакторе, а сохранять только одно. 
        [System.Serializable]
        public class States
        {
            public bool isOpen = false;
            public bool isColmplit = false;
            public int Quality = 0; 
            public int AsteroiedsDestroyed = 0;
            public int Score = 0;
        }

        public int AsteroidCount = 15;
        public float AsteroidSpeed = 5f;
        public float SpavnPeriod = 1f;
        public float FireRate = 0.5f;
        public float BulletSpeed = 5f;
        public int NeedDestroyAsteroids = 10;

        public States state;
    }
    // Боксануть, что бы сериализатор нормально сериализировал. Кастыль. 
    public class Box
    {
        public LevelConfig.States[] List;
    }

    [SerializeField]
    private LevelConfig[] LevelList;
    
    public LevelConfig[] AllLevels { get
        {
            return LevelList;
        }
    }
    //была идея, но оказалось бессмысленной, так как используется несколько сцен
    //public ReactiveCollection<LevelConfig> levelListStream;

    [SerializeField]
    private int interator = 0;
    //для нашего кастыля
    private Box box = new Box();

    public LevelConfig Current
    {
        get
        {
            return LevelList[interator];
        }
    }
    
    public int CurentIndex
    {
        get
        {
            return interator;
        }
        set
        {
            if (value < 0 || value > LevelList.Length - 1 || !LevelList[value].state.isOpen)
                return;
            interator = value;
        }
    }

    public LevelConfig Next
    {
        get
        {
            return (interator + 1 >= LevelList.Length) ? null : LevelList[interator + 1];
        }
    }

    private void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(gameObject);
            return;
        }

        instance = this;
        DontDestroyOnLoad(gameObject);

       // PlayerPrefs.SetString("_state_", "");

        LoadState();
    }

    /*
    private void Start()
    {
        //levelListStream = new ReactiveCollection<LevelConfig>(LevelList);
    }
    */

    public void FixedState(int ac, int score)
    {

        Current.state.AsteroiedsDestroyed = ac;
        Current.state.Score = score;

        Current.state.isColmplit = ac == Current.NeedDestroyAsteroids;

        Current.state.Quality = Current.state.isColmplit ? 3: 0; // тупо что бы было 
        if (Next != null)
            Next.state.isOpen = Current.state.isColmplit;
                
        SaveState();
    }

    public void GoNext()
    {
        if (interator + 1 != LevelList.Length)
            interator++;

    }

    public void SaveState()
    {

        // кастыль
        if (box.List == null)
        {
            box.List = new LevelConfig.States[LevelList.Length];
            for (int i = 0; i < box.List.Length; i++)
            {
                box.List[i] = LevelList[i].state;
            }
        }

        //var box = new Box { List = statesList };
        string state = JsonUtility.ToJson(box, true);

        //Debug
        //Debug.Log("[Level serialized]\n" + state);
        //Нельзя, но немножко можно)

        PlayerPrefs.SetString("_state_", state);
    }

    public void LoadState()
    {
        //суть  - все что не сериализированно не будет заменено и наоборот

        string state = PlayerPrefs.GetString("_state_", "");
        if (state == "")
        {
            Debug.Log("[StateLoader] State empty!");
            return;
        }
        box = JsonUtility.FromJson<Box>(state);

        //Debug
        //Debug.Log("[Level deserealized]\n" + state);

        interator = 0;
        // кастыль
        for (int i = 0; i < LevelList.Length; i++)
        {
            LevelList[i].state = box.List[i];
            if (LevelList[i].state.isOpen)
                interator = i;
        }
    }

    public void OnApplicationQuit()
    {
      //  SaveState();
    }
}

