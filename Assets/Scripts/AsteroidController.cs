﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

public class AsteroidController : MonoBehaviour {

    public static AsteroidController instance;

    private int PoolSize = 10;
    public GameObject prefab;

    private Collider2D _zone;
    private List<Transform> pool;
    private List<Transform> active = new List<Transform>();

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);
    }

    void Start() {


        _zone = GetComponent<Collider2D>();

        PoolSize = LevelConfigure.instance.Current.AsteroidCount / 2;
        CreatePool();

        GameController.instance.State
            .Where(x => x == GameController.GameState.Game)
            .Subscribe(_ => StartCoroutine(Spavn()));
    }

    void CreatePool()
    {
        pool = new List<Transform>(PoolSize);
        for (int i = 0; i < PoolSize; i++)
        {
            pool.Add((Instantiate(prefab, transform.localPosition, Quaternion.identity) as GameObject).transform);
            pool[i].gameObject.SetActive(false);
        }
    }

    public void AddNewAsteroid()
    {
        if (pool.Count == 0)
            return;

        float x = Random.Range(_zone.bounds.min.x, _zone.bounds.max.x);

        var obj = pool[0];

        pool.RemoveAt(0);
        active.Add(obj);

        obj.localPosition = new Vector3(x, transform.localPosition.y, 0);
        obj.gameObject.SetActive(true);
    }

    public void DestroyAsteroid(Transform astr, bool particles = false)
    {
        int id = active.IndexOf(astr);
        if (id >= 0)
        {
            astr.gameObject.SetActive(false);
            active.RemoveAt(id);
            pool.Add(astr);
        }
    }

    IEnumerator Spavn()
    {
        while (GameController.instance.State.Value == GameController.GameState.Game)
        {
            yield return new WaitForSeconds(LevelConfigure.instance.Current.SpavnPeriod);
            AddNewAsteroid();
        }
    }

	// Update is called once per frame
	void Update () {

        for (int i = 0; i < active.Count; i++)
        {
            var astr = active[i];
            astr.Translate(Vector3.down * Time.deltaTime * LevelConfigure.instance.Current.AsteroidSpeed);
            if (astr.position.y < _zone.bounds.min.y)
                DestroyAsteroid(astr);
        }
	}
}
