﻿//using System;
using UnityEngine;
using UnityEngine.UI;
using UniRx;
using UnityEngine.SceneManagement;

public class MapController : MonoBehaviour {

    [System.Serializable]
    public class ButtonsController
    {
        [System.Serializable]
        public class MapButton
        {
            public ReactiveProperty<bool> isActive = new ReactiveProperty<bool>(false);
            public ReactiveProperty<int> Quality = new ReactiveProperty<int>(0);
            public IObservable<int> OnClick;
            [SerializeField]
            public Button button;
            private Toggle[] toggles;
            [SerializeField]
            private int ID;
            //public System.Object Params;

            public MapButton(RectTransform _button, int id)
            {
                ID = id;
                button = _button.GetComponent<Button>();

                if (button == null)
                {
                    Debug.LogError("[MapButton constructor] компонент button не найден!");
                    return;
                }

                var _c = _button.FindChild("Status");
                toggles = new Toggle[_c.childCount];
                for (int i = 0; i < _c.childCount; i++)
                {
                    toggles[i] = _c.GetChild(i).GetComponent<Toggle>();
                }

                isActive.Subscribe(x => button.interactable = x);
                Quality.Subscribe(x =>
                {
                    for (int i = 0; i < toggles.Length; i++)
                    {
                        toggles[i].isOn = i < x;
                    }
                });

                OnClick = button.OnClickAsObservable().Select(_ => ID);
                //Debug log

                //OnClick.Subscribe(i =>
                //   {
                //       Debug.Log("[On Click] from:" + button.name + " id:" + i);
                //   });

            }

        }

        public IObservable<int> OnClicks;
        [SerializeField]
        public MapButton[] buttons;

        public ButtonsController(RectTransform container)
        {
            buttons = new MapButton[container.childCount];
            OnClicks = Observable.Empty<int>();

            for (int i = 0; i < container.childCount; i++)
            {
                buttons[i] = new MapButton(container.GetChild(i) as RectTransform, i);

                //Debug включим для проверки все
                buttons[i].isActive.Value = true;
                //Debug ну и случайное кволити 
                buttons[i].Quality.Value = Random.Range(0, 4);
                // подписываемся на все события из, тепер, по идее, все клики должны приходить к нам)
                OnClicks = OnClicks.Merge(buttons[i].OnClick);
            }
            //Debug
            //OnClicks.Subscribe(i =>
            //{
            //    Debug.Log("[On Click] from: ButtonsController"  + " id:" + i);
            //});
        }

        public void Update(LevelConfigure.LevelConfig[] configs)
        {
            for (int i = 0; i < buttons.Length; i++)
            {
                buttons[i].isActive.Value = configs[i].state.isOpen;
                buttons[i].Quality.Value = configs[i].state.Quality;
            }
        }
    }
    

    public RectTransform MapButtonsContainer;

    public ButtonsController buttonsController;

	void Start () {

        buttonsController = new ButtonsController(MapButtonsContainer);
        buttonsController.Update(LevelConfigure.instance.AllLevels);

        buttonsController.OnClicks.Subscribe(id =>
        {
            LevelConfigure.instance.CurentIndex = id;
            SceneManager.LoadScene("Game");
            
        });
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
