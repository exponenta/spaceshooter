﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using UniRx.Triggers;

// назван контроллером, но по сути это модель, управляемая с GameController
public class PlayerController : MonoBehaviour {

    
    public float MotionRange = 5f;
    public float MoveSpeed = 4f;

    private Rigidbody2D _rigid;
    private Transform _transform;
    private Vector3 _strtPos;

    void Start () {

        _rigid = GetComponent<Rigidbody2D>();
        _transform = transform;
        _strtPos = _transform.localPosition;

        GameController.instance.OnMoveTo.AddListener(MoveTo);

        // ресетим позицию
        GameController.instance.State
            .Where(s => s == GameController.GameState.Game)
            .Subscribe(_ =>
            {
                _transform.localPosition = _strtPos;
            });
        
    }

    private bool lastCheck = false;

    private void Update()
    {
        var check = Mathf.Abs(_transform.localPosition.x) >= MotionRange;
        if (lastCheck != check && check)
        {
            _rigid.velocity = Vector2.zero;
        }
        lastCheck = check;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //не будем иметь Model корабля, так как он один, так что ShipHP у нас в главном классе
        if (collision.tag == "asteroid")
        {
            GameController.instance.ShipHP.Value--;
            AsteroidController.instance.DestroyAsteroid(collision.transform);
            // еще нужно что-то делать с астероидом
        }
    }

    void StopMotion () {

            _rigid.velocity = Vector2.zero;
	}

    private void MoveTo(float direction)
    {
        //по идее нужно как-то проще, но пока я не вкурил как, так что такой хардкор
        if (Mathf.Abs(_transform.localPosition.x) < MotionRange ||
            _transform.localPosition.x > MotionRange && direction < 0 ||
            _transform.localPosition.x < -MotionRange && direction > 0 || direction == 0)

            _rigid.velocity = Vector2.right * direction * MoveSpeed;
    }
}
