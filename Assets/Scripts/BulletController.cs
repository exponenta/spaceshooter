﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

public class BulletController : MonoBehaviour
{
    private int PoolSize = 10;
    public GameObject prefab;
    public Transform SpavnPoint;
    public LayerMask DetectLayer;
     
    private Collider2D _zone;
    private List<Transform> pool;
    private List<Transform> active = new List<Transform>();

    public BoxCollider2D OverlapCollider;

    void Start()
    {
        _zone = GetComponent<Collider2D>();
        CreatePool();

        GameController.instance.State
            .Where(x => x == GameController.GameState.Game)
            .Subscribe( _=> StartCoroutine(Spavn()));
    }

    void CreatePool()
    {
        pool = new List<Transform>(PoolSize);
        for (int i = 0; i < PoolSize; i++)
        {
            pool.Add((Instantiate(prefab, transform.localPosition, Quaternion.identity) as GameObject).transform);
            pool[i].gameObject.SetActive(false);
        }
    }

    IEnumerator Spavn()
    {
        while (true)
        {
            yield return new WaitForSeconds(LevelConfigure.instance.Current.FireRate);
            if (GameController.instance.State.Value != GameController.GameState.Game)
                break;
            AddNewBullet(SpavnPoint.localPosition);// хардкор, чисто для примера
        }
    }

    public void AddNewBullet(Vector3 pos)
    {
        if (pool.Count == 0)
            return;
        
        var obj = pool[0];

        pool.RemoveAt(0);
        active.Add(obj);

        obj.localPosition = pos;
        obj.gameObject.SetActive(true);
    }

    public void DestroyBullet(Transform bull)
    {
        int id = active.IndexOf(bull);
        if (id >= 0)
        {
            bull.gameObject.SetActive(false);
            active.RemoveAt(id);
            pool.Add(bull);
        }
    }

    Collider2D overlap;
    void Update()
    {

        for (int i = 0; i < active.Count; i++)
        {
            var bull = active[i];
            bull.Translate(-Vector3.down * Time.deltaTime * LevelConfigure.instance.Current.BulletSpeed);

            
            if (bull.position.y > _zone.bounds.max.y)
            {
                DestroyBullet(bull);
            } else
            {
                overlap = Physics2D.OverlapBox(bull.localPosition, OverlapCollider.size, 0, DetectLayer.value);

                if (overlap != null && overlap.tag == "asteroid")
                {
                    GameController.instance.Destruction.Value++;

                    AsteroidController.instance.DestroyAsteroid(overlap.transform, true);
                    DestroyBullet(bull);
                }
            }
        }
    }
}
