﻿using UnityEngine;
using UniRx;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UiController : MonoBehaviour {

    // Просто такая шука, которая отключает или включает дочерние объекты
    public class HPBarController
    {
        public RectTransform Container { get; private set; }
        public int Value {
            set
            {
                Set(value);
            }

            get
            {
                return _value;
            }
        }


        private GameObject[] _dots;
        private int _value = 0;

        public HPBarController(RectTransform c)
        {
            Container = c;
            _dots = new GameObject[c.childCount];

            for (int i = 0; i < _dots.Length; i++)
            {
                _dots[i] = c.GetChild(i).gameObject;
            }

            _value = _dots.Length;
        }

        private void Set(int _new)
        {
            
            if (_new < 0 || _new > _dots.Length || _new == _value) {
                return;
            }

            int min = _new > _value ? _value : _new;
            int max = _new == min ? _value : _new;

            for (int i = min; i < max; i++)
            {
                _dots[i].SetActive(_value < _new);
            }

            _value = _new;
        }
    }

    public Text Score;
    public Text Progress;
    public RectTransform HpContainer;
    public RectTransform GameOverContainer;
    public RectTransform GameСompletedContainer;
    public RectTransform GameContainer;

    //он должен быть адаптивный =э, но у меня только 3 элемента, мне лень мудрить
    private HPBarController hpRenderer;

    void Start() {

        hpRenderer = new HPBarController(HpContainer);

        GameController.instance.Scores.Subscribe(UpdateScore);
        GameController.instance.ShipHP.Subscribe(v => hpRenderer.Value = v);
        GameController.instance.Destruction.Subscribe(UpdateProgress);

        GameController.instance.State.Subscribe( _ => {
            switch (_) {
                case GameController.GameState.GameOver:
                    GameOverContainer.gameObject.SetActive(true);
                    GameContainer.gameObject.SetActive(false);
                    break;
                case GameController.GameState.Game:
                    GameOverContainer.gameObject.SetActive(false);
                    GameСompletedContainer.gameObject.SetActive(false);
                    GameContainer.gameObject.SetActive(true);
                    break;
                case GameController.GameState.Complited:
                    GameСompletedContainer.gameObject.SetActive(true);
                    GameContainer.gameObject.SetActive(false);
                    break;
            }
        });

    }

    //UI/GameOver/Restart 
    // Не хочу делать ссылку на кнопку. Простите
    public void RestartClicked() {

        // Вот так
        
        GameController.instance.Start();
    }

    //UI/Complited/Next
    // Не хочу делать ссылку на кнопку. Простите
    public void NextClicked()
    {
        LevelConfigure.instance.GoNext();
        // Вот так
        GameController.instance.Start();
    }
    //UI/*/Menu
    // Не хочу делать ссылку на кнопку. Простите
    public void MenuClicked()
    {
        LevelConfigure.instance.SaveState();
        SceneManager.LoadScene("MainMenu");
    }

    public void UpdateScore(int val)
    {
        Score.text = val.ToString("D4");
    }

    public void UpdateProgress(int val)
    {
        Progress.text = LevelConfigure.instance.Current.NeedDestroyAsteroids.ToString() + "/" + val.ToString();
    }

}
