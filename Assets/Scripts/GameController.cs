﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UniRx;
using UniRx.Triggers;

public class GameController : MonoBehaviour
{
    public enum GameState { Menu, Game, GameOver, Complited };

    public static GameController instance;

    [System.Serializable]
    public class OnInput : UnityEvent<float> { };
    public OnInput OnMoveTo;

    public ReactiveProperty<GameState> State = new ReactiveProperty<GameState>(GameState.Game);

    //нужно по идее иметь это поле в модели корабля, но он один (нам не нужно много, вот если много - другой вопрос)
    public ReactiveProperty<int> ShipHP = new ReactiveProperty<int>(3);
    public ReactiveProperty<int> Scores = new ReactiveProperty<int>(0);
    public ReactiveProperty<int> Destruction = new ReactiveProperty<int>(0);


    private void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);

        ShipHP.Where(hp => hp == 0).Subscribe(_ => State.Value = GameState.GameOver);

        State.Subscribe(x => Debug.Log("[State]" + x.ToString()));

        Destruction.Where(d => d > 0).Subscribe(_ =>
           {
               Scores.Value += 10;
           });

        Destruction
            .Where(d => d == LevelConfigure.instance.Current.NeedDestroyAsteroids)
            .Subscribe(_ => State.Value = GameState.Complited);

        State.Where(s => s != GameState.Game).Subscribe(_ => LevelConfigure.instance.FixedState(Destruction.Value, Scores.Value));

        /*
        // Можно все сделать на потоке (но кому-то не понравилось)

        UpdateStream = Observable.EveryUpdate();

        var fromKeys = UpdateStream.Select(_ => Input.GetAxisRaw("Horizontal"));
        var fromTouch = UpdateStream.Where(_ => Input.touchCount > 0)
            .Select(_ => 2f * (Input.touches[0].position.x - 0.5f));
        
        MotionStream = fromKeys
            .Merge(fromTouch)
            .Where(_ => State.Value != GameState.GameOver)
            .Select(x => (float)System.Math.Sign(x))
            .Merge(State.Where(x => x == GameState.GameOver).Select(x_ => 0.0f)) // если GameOver, то посылаем 0, что бы остановить
            .DistinctUntilChanged(); // передаем только изменения, что бы не засирать поток
            
        //Debug
            MotionStream.Subscribe(x => Debug.Log("[Input]" + x));
            MotionStream.Subscribe(OnMoveTo.Invoke);
        */
    }

    public void Start()
    {

        ShipHP.Value = 3;
        Scores.Value = 0;
        Destruction.Value = 0;
        State.Value = GameState.Game;
        lastState = GameState.Game;
    }

    // что бы дебажить
    public void DebugClick(int val)
    {
        ShipHP.Value = val;
    }

    private float lastInput = 0;
    private GameState lastState = GameState.Game;

    void Update()
    {

        // Тут будет стандартный контролл, более геморойно, чем если реактивно

        if (State.Value == GameState.Game)
        {
            float input = 0;

            // по идее нужно выбрать тип ввода в зависимости от устройства, но вдруг я джой подцеплю к мобилке? упс
            if (Input.touchCount > 0)
                input = (Input.touches[0].position.x - 0.5f) * 2f;
            else
                input = Input.GetAxisRaw("Horizontal");

            input = System.Math.Sign(input);// в Unity Sign нету 0, как не странно

            if (input != lastInput)
                OnMoveTo.Invoke(input);

            lastInput = input;
        } else
        {
            // можно конечно подписатся, и этого не делать, но если писать на if, то уже все. 
            if (lastState != State.Value)
            {
                OnMoveTo.Invoke(0);
                lastState = State.Value;
            }
        }

    }
}
